var t = TrelloPowerUp.iframe();

window.estimate.addEventListener('submit', function(event) {
  event.preventDefault();
  return t.set('card', 'shared', 'estimate_front', window.estimate_frontSize.value)
    .then(function() {
      t.set('card', 'shared', 'estimate_back', window.estimate_backSize.value)
        .then(function() {
          t.closePopup();
        });
    });

});

t.render(function() {

  t.get('card', 'shared', 'estimate_front')
    .then(function(estimate_front) {
      window.estimate_frontSize.value = estimate_front;
    });
  t.get('card', 'shared', 'estimate_back')
    .then(function(estimate_back) {
      window.estimate_backSize.value = estimate_back;
    });

  t.sizeTo('#estimate').done();
});

