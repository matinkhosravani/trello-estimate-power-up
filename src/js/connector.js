var GRAY_ICON = 'https://cdn.hyperdev.com/us-east-1%3A3d31b21c-01a0-4da2-8827-4bc6e88b7618%2Ficon-gray.svg';
var GREY_ROCKET_ICON = 'https://cdn.glitch.com/c69415fd-f70e-4e03-b43b-98b8960cd616%2Frocket-ship-grey.png?1496162964717';
var WHITE_ROCKET_ICON = 'https://cdn.glitch.com/c69415fd-f70e-4e03-b43b-98b8960cd616%2Fwhite-rocket-ship.png?1495811896182';
window.TrelloPowerUp.initialize({

  'card-badges': function(t, options) {
    return t.get('card', 'shared', 'estimate_back')
      .then(function(estimate_back) {
        return t.get('card', 'shared', 'estimate_front')
          .then(function(estimate_front) {
            return [{
              icon: estimate_back ? GREY_ROCKET_ICON : WHITE_ROCKET_ICON,
              text: estimate_back || 'No Back Estimate!',
              color: estimate_back ? 'green' : 'red',
            }, {
              icon: estimate_front ? GREY_ROCKET_ICON : WHITE_ROCKET_ICON,
              text: estimate_front || 'No Front Estimate!',
              color: estimate_front ? 'orange' : 'red',
            }];
          });
      });
  },
  'card-detail-badges': function(t, options) {
    return t.get('card', 'shared', 'estimate_back')
      .then(function(estimate_back) {
        return t.get('card', 'shared', 'estimate_front')
          .then(function(estimate_front) {
            return [{
              title: 'Estimate',
              text: estimate_back || 'No Back Estimate!',
              color: estimate_back ? 'green' : 'red',
              callback: function(t) {
                return t.popup({
                  title: 'Estimation',
                  url: 'estimate.html',
                });
              },
            }, {
              title: 'Estimate',
              text: estimate_front || 'No Front Estimate!',
              color: estimate_front ? 'orange' : 'red',
              callback: function(t) {
                return t.popup({
                  title: 'Estimation',
                  url: 'estimate.html',
                });
              },
            }];
          });
      });
  },
  'card-buttons': function(t, options) {
    return [{
      icon: 'https://cdn.glitch.com/1b42d7fe-bda8-4af8-a6c8-eff0cea9e08a%2Frocket-ship.png?1494946700421',
      text: 'Estimate Size',
      callback: function(t) {
        return t.popup({
          title: 'Estimation',
          url: 'estimate.html',
        });
      },
    }];
  },
  'show-settings': function(t, options) {
    // when a user clicks the gear icon by your Power-Up in the Power-Ups menu
    // what should Trello show. We highly recommend the popup in this case as
    // it is the least disruptive, and fits in well with the rest of Trello's UX
    return t.popup({
      title: 'Custom Fields Settings',
      url: './settings.html',
      height: 184, // we can always resize later
    });
  },
});
